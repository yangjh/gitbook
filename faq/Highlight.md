# 如何实现代码的高亮功能

在GitBook生成的静态文档中，默认的代码样式没有高亮，不够醒目。这是由于在书写md时，没有指定代码语言。指定特定语言后，即可实现高亮：

```html
<p>test</p>
```

上述效果的代码如下：

    ```html
    <p>test</p>
    ```

## highlight插件支持的语法

`gitbook-plugin-highlight` 使用 `highlight.js` 作为后台渲染引擎，`highlight.js` 支持的语言及简写可在[官方手册][1]中查看。

[1]: http://highlightjs.readthedocs.io/en/latest/css-classes-reference.html#language-names-and-aliases

## 使用prism插件

系统自带插件的高亮功能并不完善，可使用prism插件增强，该插件需要先禁用`highlight`插件。

在配置文件中增加如下信息：

```json
{
   "plugins": ["-highlight", "prism", "prism-themes"]
}
"pluginsConfig": {
  "prism": {
    "css": [
      "prismjs/themes/prism-solarizedlight.css"
    ]
  }
}
```

使用`prism`高亮插件的优点在于，可以使用不同的配色方案，且语法关键词识别度比`highlight`插件高。`prism`支持的语言可在其[官方网站](https://prismjs.com/#languages-list)查询。

# 如何引入外部文件

在有些情景下，我们需要将外部独立的文件加入到文档中。[include codeblock插件][1]可以满足这个需求。

## 安装并配置插件

include Codeblock插件需要和ace插件配合，因此，在book.json文件中，加入如下内容：

```json
"plugins": [
        "include-codeblock",
        "ace"
    ],
"include-codeblock": {
    "template": "ace",
    "unindent": true,
    "edit": true
    }
```

## 在文档中引入外部文件

[import](include.md)

更加详细的用法，可以查看[include codeblock插件手册][1]。

[1]: https://plugins.gitbook.com/plugin/include-codeblock

## 引入并渲染外部文件

### 引入并渲染本地外部文件

Gitbook还提供了引入并渲染外部文件的功能，使用方法如下：

```markdown
{% include "../SUMMARY.md" %}
```

### 引入并渲染git仓库中的外部文件

Gitbook还可以引入并渲染Git仓库中的文件内容片段：

```markdown
{% include "git+https://github.com/GitbookIO/documentation.git/README.md#0.0.1" %}
```

该命令会将同级目录中的文件内容，插入到当前位置，并进行解析渲染。效果如下：

{% include "git+https://github.com/GitbookIO/documentation.git/README.md#0.0.1" %}

# 为左侧导航栏添加链接信息

在`book.json`文件中，添加`links`配置项：

```json
"links" : {
    "sidebar" : {
        "Home" : "http://yangzh.cn"
    }
}
```

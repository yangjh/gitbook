# 如何插入外部页面

有时候，需要在页面中插入外部的独立页面。如演示页面等。可使用`url-embed`插件达到上述目的：

## 安装

在book.json中加入如下内容，然后运行`gitbook install`:

```json
{
    "plugins": ["url-embed"]
}
```

## 用法

这个插件将以iframe的形式，在当前页面插入页面内容。在页面中，使用如下内容指定内容所在地址：

```markup-templating
{% urlembed %}
https://website.org/stuff/this-is-the-path-name
{% endurlembed %}
```

## 效果

{% urlembed %}
https://plugins.gitbook.com/plugin/url-embed
{% endurlembed %}
